var nga = require('../lib/nga.js')
  , should = require('should');

describe('Call nga', function () {
  describe('With no args', function () {
    it('Has no props propId and hostName', function () {
      var result = new nga();

      result.should.not.have.property('propId');
      result.should.not.have.property('hostName');
    });
  });
});

describe('Call nga', function () {
  describe('With one arg', function () {
    it('Has the prop propId only', function () {
      var result = new nga('UA-XXXXX-Y');

      result.should.not.have.property('hostName');
    });
  });
});

describe('Call nga', function () {
  describe('With args', function () {
    it('Has the props propId and hostName', function () {
      var result = new nga('UA-XXXXX-Y', 'sgaas.io');

      result.should.have.property('propId');
      result.should.have.property('hostName');
    });
  });
});

describe('Call nga', function () {
  describe('With no args', function () {
    var ga = new nga();

    describe('Call nga.hashHostName', function () {
      it('Returns false', function () {
        var result = ga.hashHostName();

        result.should.equal(false);
      });
    });
  });

  describe('With args', function () {
    var ga = new nga('UA-XXXXX-Y', 'sgaas.io');

    describe('Call nga.hashHostName', function () {
      it('Returns 17103419', function () {
        var result = ga.hashHostName();

        result.should.equal(17103419);
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[2] or args[3])', function () {
    var ga = new nga('UA-XXXXX-Y', 'sgaas.io');

    describe('Req nga.__utma', function () {
      it('Returns string', function () {
        var result = ga.__utma;

        /*console.log('__utma: ' + ga.__utma);
        console.log('__utmb: ' + ga.__utmb);
        console.log('__utmz: ' + ga.__utmz);*/

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[3])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma);

    describe('Req nga.__utma', function () {
      it('Returns array', function () {
        var result = ga.__utma;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With erroneous args (but with no args[3])', function () {
    var erroneous__utma = '17103419.1454752900.1370174229.1370174229.1370174229'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', erroneous__utma);

    describe('Req nga.__utma', function () {
      it('Returns array', function () {
        var result = ga.__utma;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmb = '17103419.1.10.1370251341'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmb);

    describe('Req nga.__utmb', function () {
      it('Returns array', function () {
        var result = ga.__utmb;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With erroneous args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , erroneous__utmb = '17103419.1.10'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, erroneous__utmb);

    describe('Req nga.__utmb', function () {
      it('Returns array', function () {
        var result = ga.__utmb;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmz = '118033035.1370250200.1.1.utmcsr=(direct)|utmccn=(direct)|utmc'
        + 'md=(none)'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmz);

    describe('Req nga.__utmz', function () {
      it('Returns array', function () {
        var result = ga.__utmb;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmb = '17103419.1.10.1370251341'
      , __utmz = '118033035.1370250200.1.1.utmcsr=(direct)|utmccn=(direct)|utmc'
        + 'md=(none)'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmb, __utmz);

    describe('Req nga.__utmz', function () {
      it('Returns array', function () {
        var result = ga.__utmz;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With erroneous args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmz = '118033035.1370250200.1.1'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmz);

    describe('Req nga.__utmz', function () {
      it('Returns array', function () {
        var result = ga.__utmz;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With erroneous args', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmb = '17103419.1.10.1370251341'
      , __utmz = '118033035.1370250200.1.1'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmb, __utmz);

    describe('Req nga.__utmz', function () {
      it('Returns array', function () {
        var result = ga.__utmz;

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmz = '118033035.1370250200.1.1.utmcsr=(direct)|utmccn=(direct)|utmc'
        + 'md=(none)'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmz);

    describe('Call nga.update__utma', function () {
      it('Returns array', function () {
        var result = ga.update__utma();

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmz = '118033035.1370250200.1.1.utmcsr=(direct)|utmccn=(direct)|utmc'
        + 'md=(none)'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmz);

    describe('Call nga.update__utmb', function () {
      it('Returns array', function () {
        var result = ga.update__utmb();

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[4])', function () {
    var __utma = '17103419.1454752900.1370174229.1370174229.1370174229.1'
      , __utmz = '118033035.1370250200.1.1.utmcsr=(direct)|utmccn=(direct)|utmc'
        + 'md=(none)'
      , ga = new nga('UA-XXXXX-Y', 'sgaas.io', __utma, __utmz);

    describe('Call nga.update__utmz', function () {
      it('Returns array', function () {
        var result = ga.update__utmz();

        result.should.be.a('object');
      });
    });
  });
});

describe('Call nga', function () {
  describe('With args (but with no args[2, 3, 4])', function () {
    var ga = new nga('UA-41360951-1', 'sgaas.io');

    describe('Call nga.trackPageview', function () {
      it('Returns string', function () {
        var result = ga.trackPageview('/');

        console.log(result);

        result.should.be.a('string');
      });
    });
  });
});