var ga = require('../lib/ga.js')
  , should = require('should');

describe('ga', function () {
  describe('With no args', function () {
    it('returns false', function () {
      var result = ga();

      result.should.eql(false);
    });
  });
});