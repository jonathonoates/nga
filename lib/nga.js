module.exports = function (propId, hostName, __utma, __utmb, __utmz) {
  var that = this;

  that.args = arguments;
  that.hostName = hostName ? encodeURIComponent(hostName) : undefined;

  that.hashHostName = function () {
    if (that.hostName === undefined) {
      return false;
    }

    var domainHash = 1
      , characterCode
      , i = that.hostName.length - 1
      , j;

    if (that.hostName) {
      domainHash = 0;

      for (; i >= 0; i--) {
        characterCode = that.hostName.charCodeAt(i);
        domainHash = (domainHash << 6 & 268435455) + characterCode
          + (characterCode << 14);
        j = domainHash & 266338304;

        if (j != 0) {
          domainHash = domainHash ^ j >> 21;
        }
      }
    }

    return domainHash;
  };
  that.hashedHostName = that.hashHostName();

  that.now = function () {
    return new Date().getTime().toString().substring(0, 10);
  };

  that.propId = propId; // eg. 'UA-XXXXX-Y'
  that.then = that.now();

  that.__utma = (function () {
    var now = that.then
      , value = that.args[2] ? that.args[2].split('.') : undefined;

    function newUId () { // TODO: 
      return new Date().getTime().toString().substr(-11);
    }

    if (value === undefined || value.length !== 6) {
      return [that.hashedHostName, newUId(), now, now, now, 1];
    } else {
      return value;
    }
  }());

  that.update__utma = function () {
    that.__utma[3] = that.__utma[4];
    that.__utma[4] = that.now();
    that.__utma[5] = ++that.__utma[5];

    return that.__utma;
  };

  that.__utmz = (function () {
    var campaignParams
      , now = that.then
      , value = that.args[4] ? that.args[4].split('.') : undefined;

    value = value || that.args[3] ? that.args[3].split('.') : undefined

    if (value && value.length === 5) {
      campaignParams = value.pop().split('|');

      return value.concat(campaignParams);
    } else {
      return [that.hashedHostName, now, 1, 1, 'utmcsr=(direct)',
        'utmccn(direct)', 'utmcmd(none)'];
    }
  }());

  that.update__utmz = function () { // TODO: Update __utmz[3] and __utmz[4] too
    that.__utmz[2] = ++that.__utmz[2];

    return that.__utmz;
  };

  that.__utmb = (function () {
    var now = that.then
      , value = that.args[3] ? that.args[3].split('.') : undefined;

    if (value === undefined || value.length !== 4) {
      if (that.__utma[2] !== now && that.__utmz[1] !== now) {
        that.update__utma();
        that.update__utmz();
      }

      return [that.hashedHostName, 1, 10, now];
    } else {
      return value;
    }
  }());

  that.update__utmb = function () {
    that.__utmb[1] = ++that.__utmb[1];

    return that.__utmb;
  };

  that.req__utmGif = function (parameters) {
    var i = 0
      , params = parameters || {}
      , queryString = []
      , reqUrl = 'http://www.google-analytics.com/__utm.gif?'
      , separator = '.';

    params.utmv = 5;
    params.utmn = that.now().substring(0, 10);
    params.utmhn = that.hostName;
    params.utmcs = '-';
    params.utmr = 0; // Referral
    params.utmac = that.propId;
    params.utmcc = '__utma=' + that.__utma.join('.') + ';+__utmb=' + that.__utmb
      .join('.') + ';+__utmz='

    for (; i < that.__utmz.length; i++) {
      if (i === 4) {
        separator = '|';
      }

      params.utmcc += that.__utmz[i] + separator;
    }

    params.utmcc += ';'

    for (var param in params) {
      queryString.push(param + '=' + encodeURIComponent(params[param]));
    }

    return reqUrl += queryString.join('&');
  };

  that.trackPageview = function (virtualPageview) {
    var req = that.req__utmGif({ utmp: virtualPageview });
    that.update__utmb();

    return req;
  };

  return that;
};